import React, { Component } from 'react';
import {
    View, StyleSheet, Text, Image, TouchableOpacity, FlatList, ActivityIndicator
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Home extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        }
    }

    componentDidMount() {
        this.getLigaTeams();
    }

    getLigaTeams = async() => {
        try {
            const response = await Axios.get(`https://www.thesportsdb.com/api/v1/json/1/search_all_teams.php?l=Indonesian%20Super%20League`);
            this.setState({isError: false, isLoading: false, data: response.data.teams});
        } catch (error) {
            this.setState({isLoading: false, isError: true});
        }
    }

    render() {
        const { navigation } = this.props;
        if(this.state.isLoading) {
            return (
                <View style={Styles.container}>
                    <ActivityIndicator size="large" color="#EE4D2D" />
                </View>
            )
        } else if(this.state.isError) {
            return (
                <View style={Styles.container}>
                    <Text>Terjadi error saat memuat data</Text>
                </View>
            )
        }
        return (
            <View style={Styles.container}>
                <FlatList
                    data={this.state.data}
                    keyExtractor={({idTeam}, i) => idTeam}
                    renderItem={({item}) => (
                        <TouchableOpacity style={Styles.listContainer} onPress={() => navigation.navigate('Detail', {id: item.idTeam, name: item.strTeam})}>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Image source={{uri:item.strTeamBadge}} style={Styles.logoTeams} />
                                <View style={{paddingLeft: 18}}>
                                    <Text style={Styles.nameTeams}>{item.strTeam}</Text>
                                    <Text style={Styles.nameStadiums}>{item.strStadium}</Text>
                                </View>
                            </View>
                            <View style={{flexDirection: "row"}}>
                                <Icon name="long-arrow-right" size={14} color="#6C6C6C" />
                            </View>
                        </TouchableOpacity>
                    )}
                    style={Styles.insideContainer}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    },
    insideContainer: {
        width: "96%"
    },
    listContainer: {
        borderBottomColor: "#D8D8D8",
        borderBottomWidth: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    logoTeams: {
        width: 36,
        height: 36
    },
    nameTeams: {
        color: "#2F2F2F",
        fontWeight: "bold",
        fontSize: 20
    },
    nameStadiums: {
        color: "#2F2F2F",
        fontSize: 12
    }
})