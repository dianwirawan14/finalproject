import React, { Component } from 'react';
import {
    View, StyleSheet, Text, Image, TouchableOpacity, FlatList, ActivityIndicator, Linking
} from 'react-native';
import Axios from 'axios';
import Icon from 'react-native-vector-icons/FontAwesome';
import MIcon from 'react-native-vector-icons/MaterialCommunityIcons';

export default class Detail extends Component {
    constructor(props) {
        super(props);
        this.state = {
            data: {},
            isLoading: true,
            isError: false
        }
    }

    componentDidMount() {
        this.getLigaTeams();
    }

    getLigaTeams = async() => {
        try {
            const response = await Axios.get(`https://www.thesportsdb.com/api/v1/json/1/lookupteam.php?id=${this.props.route.params.id}`);
            this.setState({isError: false, isLoading: false, data: response.data.teams});
        } catch (error) {
            this.setState({isLoading: false, isError: true});
        }
    }

    render() {
        if(this.state.isLoading) {
            return (
                <View style={Styles.container}>
                    <ActivityIndicator size="large" color="#EE4D2D" />
                </View>
            )
        } else if(this.state.isError) {
            return (
                <View style={Styles.container}>
                    <Text>Terjadi error saat memuat data</Text>
                </View>
            )
        }
        return (
            <View style={Styles.container}>
                <FlatList
                    data={this.state.data}
                    keyExtractor={({idTeam}, i) => idTeam}
                    renderItem={({item}) => (
                        <View style={Styles.body}>
                            <View style={Styles.badgeContainer}>
                                <Image source={{uri:item.strTeamBadge}} style={Styles.logoTeams} />
                            </View>
                            <Text style={Styles.nameTeams}>{item.strTeam}</Text>
                            <View style={{paddingTop: 17, paddingBottom: 13, flexDirection: "row", alignItems: "center"}}>
                                <MIcon name="stadium-variant" size={14} color="#2F2F2F" />
                                <Text style={Styles.nameStadiums}>{item.strStadium} ({nFormatter(item.intStadiumCapacity)})</Text>
                            </View>
                            <View style={{flexDirection: "row", alignItems: "center"}}>
                                <Icon name="home" size={14} color="#2F2F2F" />
                                <Text style={{fontWeight: "bold", color: "#2F2F2F", fontSize: 14, marginLeft: 5}}>Official Website</Text>
                                <TouchableOpacity onPress={() => Linking.openURL(`https://${item.strWebsite}`)}>
                                    <Text style={Styles.nameStadiums}>{item.strWebsite}</Text>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection: "row", marginTop: 10}}>
                                <TouchableOpacity style={Styles.social} 
                                    onPress={() => Linking.openURL(`https://${item.strFacebook}`)} >
                                    <Icon name="facebook-square" size={16} style={{color: "#2F80ED"}} />
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.social} 
                                    onPress={() => Linking.openURL(`https://${item.strTwitter}`)} >
                                    <Icon name="twitter" size={16} style={{color: "#56CCF2"}} />
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.social} 
                                    onPress={() => Linking.openURL(`https://${item.strYoutube}`)} >
                                    <Icon name="youtube-play" size={16} style={{color: "#FF0000"}} />
                                </TouchableOpacity>
                                <TouchableOpacity style={Styles.social} 
                                    onPress={() => Linking.openURL(`https://${item.strInstagram}`)} >
                                    <Icon name="instagram" size={16} style={{color: "#BB6BD9", backgroundColor: "white"}} />
                                </TouchableOpacity>
                            </View>
                            <View style={{marginTop: 20, backgroundColor: "#FFFFFF", padding: 5}}>
                                <Text style={{textAlign: "center"}}>{item.strDescriptionEN}</Text>
                            </View>
                        </View>
                    )}
                    style={Styles.insideContainer}
                    showsHorizontalScrollIndicator={false}
                    showsVerticalScrollIndicator={false}
                />
            </View>
        )
    }
}

let nFormatter = (num) => {
    return num.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },
    body: {
        justifyContent: "center",
        alignItems: "center"
    },
    insideContainer: {
        width: "98%"
    },
    listContainer: {
        borderBottomColor: "#D8D8D8",
        borderBottomWidth: 1,
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 5
    },
    logoTeams: {
        width: 90,
        height: 90
    },
    nameTeams: {
        color: "#2F2F2F",
        fontSize: 24,
        fontWeight: "bold"
    },
    nameStadiums: {
        color: "#2F2F2F",
        fontSize: 14,
        marginLeft: 5
    },
    badgeContainer: {
        backgroundColor: "#FFFFFF",
        height: 120,
        width: 120,
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 60,
        elevation: 3,
        marginVertical: 20
    },
    social: {
        marginHorizontal: 10,
        width: 40,
        height: 40,
        backgroundColor: "#FFFFFF",
        borderRadius: 20,
        alignItems: "center",
        justifyContent: "center",
        elevation: 3
    }
})