import React from 'react';
import {
    View, Text, StyleSheet, Image, TouchableOpacity, Linking
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

const About = ({navigation}) => {
    return(
        <View style={Styles.container}>
            <View style={Styles.body}>
                <Image source={require('../assets/about.jpg')} resizeMode="contain" style={{
                    width: "50%",
                    height: "40%"
                }} />
                <Text style={{
                    fontSize: 24,
                    fontWeight: "bold"
                }}>Gede Dian Wirawan Putra</Text>
                <Text style={{
                    color: "#8F8F8F"
                }}>Web and React Native Developer</Text>
                <View style={{
                    marginTop: "7%",
                    flexDirection: "row"
                }}>
                    <TouchableOpacity style={Styles.social} 
                        onPress={() => Linking.openURL('https://www.instagram.com/dianwirawan14/')} >
                        <Icon name="instagram" size={25} style={{color: "#BB6BD9"}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.social} 
                        onPress={() => Linking.openURL('https://web.facebook.com/dian.wirawan.14/')} >
                        <Icon name="facebook-square" size={25} style={{color: "#2F80ED"}} />
                    </TouchableOpacity>
                    <TouchableOpacity style={Styles.social} 
                        onPress={() => Linking.openURL('https://twitter.com/dianwirawan14')} >
                        <Icon name="twitter-square" size={25} style={{color: "#56CCF2"}} />
                    </TouchableOpacity>
                </View>
            </View>
        </View>
    )
}
const Styles = StyleSheet.create({
    container: {
        flex: 1
    },
    social: {
        marginHorizontal: 10
    },
    body: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
    }
})

export default About;