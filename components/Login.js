import React, { Component } from 'react';
import {
    View, StyleSheet, Text, Image, TextInput, Button
} from 'react-native';
import Constants from 'expo-constants';
import { connect } from "react-redux";
import { types, screen } from "../redux/types";


class Login extends Component {
    constructor() {
        super();
    }
    
    render() {
        const { navigation } = this.props;
        let error = <View></View>;
        if (this.props.isError) {
            error = (
                <Text style={{ color: "red", alignSelf: "center" }}>
                    Username atau password salah
                </Text>
            );
        }

        return (
            <View style={Styles.container}>
                <Image source={require('../assets/logo.png')} style={{
                    width: 128,
                    height: 128
                }} />
                <View style={Styles.inputContainer}>
                    <Text>Username</Text>
                    <TextInput style={Styles.inputDesign} placeholder="admin"
                        onChangeText={(text) => {
                            this.props.setUsername(text);
                        }}
                    />
                </View>
                <View style={Styles.inputContainer}>
                    <Text>Password</Text>
                    <TextInput style={Styles.inputDesign} secureTextEntry={true} placeholder="admin"
                        onChangeText={(text) => {
                            this.props.setPassword(text);
                        }}
                    />
                </View>
                <View style={Styles.inputContainer}>
                    <Button title="Login" color="#EE4D2D"
                        onPress={() => {
                            console.log(this.props)
                            if (this.props.username === "admin" && this.props.password === "admin") {
                                this.props.setError(false);
                                this.props.navigation.reset({
                                    index: 0,
                                    routes: [{name: 'Home'}],
                                });
                            } else {
                                this.props.setError(true);
                            }
                        }}
                    />
                    {error}
                </View>
            </View>
        )
    }
}

const Styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: Constants.statusBarHeight
    },
    inputDesign: {
        borderBottomColor: "#b6b6b6",
        borderBottomWidth: 1,
        fontSize: 18,
        height: 40
    },
    inputContainer: {
        marginTop: "5%",
        width: "90%"
    }
})

const mapStateToProps = (state) => {
    return {
        username: state.login.username,
        password: state.login.password,
        isError: state.login.isError,
    };
};

const mapDispatchToProps = (dispatch) => {
    return {
        setUsername: (username) => {
            dispatch({ type: types.INPUT_USERNAME, screen: screen.LOGIN, username });
        },
        setPassword: (password) => {
            dispatch({ type: types.INPUT_PASSWORD, screen: screen.LOGIN, password });
        },
        setError: (isError) => dispatch({ type: types.ERROR, screen: screen.LOGIN, isError }),
    };
};

LoginScreen = connect(mapStateToProps, mapDispatchToProps)(Login);
export default LoginScreen;
  