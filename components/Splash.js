import React, { useEffect } from 'react';
import { View, StyleSheet, Image } from 'react-native';
import Constants from 'expo-constants';

const Splash = ({ navigation }) => {
    useEffect(() => {
        setTimeout(() => {
            navigation.reset({
                index: 0,
                routes: [{ name: 'Login' }]
            });
        }, 3000);
    });

    return (
        <View style={Styles.home}>
            <Image source={require('../assets/logo.png')} resizeMode="contain" style={{
                width: 128
            }} />
        </View>
    )
};
const Styles = StyleSheet.create({
    home: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        marginTop: Constants.statusBarHeight
    }
})
export default Splash;