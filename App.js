import React from "react";
import { Image, TouchableOpacity } from "react-native";
import { DrawerActions, NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import { createDrawerNavigator } from "@react-navigation/drawer";
import Icon from 'react-native-vector-icons/FontAwesome';
import { createStore } from 'redux'
import { Provider } from 'react-redux'
import reducer from './redux/reducer'

import Splash from "./components/Splash";
import Login from "./components/Login";
import About from "./components/About";
import HomeScreen from "./components/Home";
import Detail from "./components/Detail";

const Stack = createStackNavigator();
const Drawer = createDrawerNavigator();

const DrawerContent = () => {
    return (
        <Drawer.Navigator initialRouteName="Home">
            <Drawer.Screen name="Home" component={HomeScreen} />
            <Drawer.Screen name="About" component={About} />
        </Drawer.Navigator>
    )
}

const Logo = () => {
    return (
        <Image source={require('./assets/logo.png')} style={{width: 48, height: 48}} />
    )
}

const StackScreen = () => (
    <Stack.Navigator initialRouteName="Splash">
        <Stack.Screen name="Splash" component={Splash} options={{
            headerShown: false
        }} />
        <Stack.Screen name="Login" component={Login} options={{
            headerShown: false
        }} />
        <Stack.Screen name="Home" component={DrawerContent} options={({navigation}) => ({
            headerTitle: props => <Logo {...props} />,
            headerLeft: null,
            headerStyle: {
                backgroundColor: "#EE4D2D"
            },
            headerRight: () => (<TouchableOpacity onPress={() => navigation.dispatch(DrawerActions.toggleDrawer())} style={{marginRight: 14}}>
                <Icon name="bars" size={25} color="#FFF" />
            </TouchableOpacity>)
        })} />
        <Stack.Screen name="Detail" component={Detail} options={
            ({ route }) => ({ 
                title: route.params.name,
                headerStyle: {
                    backgroundColor: "#EE4D2D"
                },
                headerTintColor: "#FFF"
            })
        } />
    </Stack.Navigator>
)

export default function App() {
    const store = createStore(reducer)
    return (
        <Provider store={store}>
            <NavigationContainer>
                <StackScreen />
            </NavigationContainer>
        </Provider>
    );
}